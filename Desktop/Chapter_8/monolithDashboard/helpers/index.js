const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const {user} = require('../models')


passport.use(new LocalStrategy(async (username, password, done) => {
    console.log(username , "<< ini name")
    let users = await user.findOne({
        where : {
            name : username
        }
    })

    if(user) {
        done(null, users)
    }else {
        done(null, null)
    }

}))
passport.serializeUser((user, done) => {
    console.log(user, '<< ini users')
    console.log(user.id, '<< Ini yang akan disimpan di session')
    done(null, user.id)
} )
passport.deserializeUser(function(user, done) { //Here you retrieve all the info of the user from the session storage using the user id stored in the session earlier using serialize user.
    console.log(user, "<< ini user")
    done(null,{id : user})
});

module.exports = {passport}