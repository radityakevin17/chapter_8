'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class news extends Model {
    static associate(models) {
      this.belongsTo(models.user, {foreignKey : 'userId'})
    }
  }
  news.init({
    title: DataTypes.TEXT,
    isReleased: DataTypes.BOOLEAN,
    desc: DataTypes.TEXT,
    body: DataTypes.TEXT,
    userId : DataTypes.STRING
  }, {
    sequelize,
    modelName: 'news',
  });
  return news;
};