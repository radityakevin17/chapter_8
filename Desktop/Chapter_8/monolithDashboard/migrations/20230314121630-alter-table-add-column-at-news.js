'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('news', 'userId' , {type : Sequelize.STRING})
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.dropColumn('news', 'userId', {})
  }
};
